use std::collections::HashMap;
use std::io::prelude::*;
use std::fs::File;
use glium::Program;
use glium_sdl2::SDL2Facade;
use glium;

const SHADER_PATHS: [&'static str; 1] = ["basic"];

pub struct ProgramCache {
    programs: HashMap<String, Program>
}

impl ProgramCache {
    pub fn new(sdl_facade: &SDL2Facade, base_directory: &String) -> ProgramCache {
        let mut cache = HashMap::new();

        for path in SHADER_PATHS.iter() {
            let vertex_shader = ProgramCache::read_file(base_directory.as_str().to_owned() + path + "/vert.glsl");
            let frag_shader = ProgramCache::read_file(base_directory.as_str().to_owned() + path + "/frag.glsl");
            let program = glium::Program::from_source(sdl_facade, vertex_shader.as_str(), frag_shader.as_str(), None).unwrap();
            cache.insert(path.to_string(), program);
        }

        ProgramCache {
            programs: cache,
        }
    }

    pub fn get_program(&self, name: &String) -> &Program {
        &self.programs.get(name).unwrap()
    }

    pub fn read_file(path: String) -> String {
        let mut file = File::open(path).unwrap();
        let mut buffer = String::new();
        assert!(file.read_to_string(&mut buffer).is_ok());
        buffer
    }
}