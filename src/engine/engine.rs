use sdl2::EventPump;
use sdl2::Sdl;
use glium::Surface;
use glium_sdl2::SDL2Facade;
use std::vec::Vec;

use super::program_cache::ProgramCache;
use game::game_states::GameState;
use game::game_states::menu_state::MenuState;
use engine::rendering::factory::Factory;
use engine::input_handler::InputHandler;

pub struct Engine<'a> {
    running: bool,
    display: &'a SDL2Facade,
    input_handler: InputHandler,
    program_cache: ProgramCache,
    mesh_factory: Factory<'a>,
    game_states: Vec<Box<GameState>>,
}

impl<'a> Engine<'a> {
    pub fn new(sdl_context: &mut Sdl, display: &'a SDL2Facade) -> Engine<'a> {
        let mut engine = Engine {
            running: true,
            program_cache: ProgramCache::new(display, &"./resources/shaders/".to_string()),
            mesh_factory: Factory::new(display),
            display: display,
            input_handler: InputHandler::new(sdl_context.event_pump().unwrap()),
            game_states: Vec::new(),
        };

        engine.push_state(MenuState::new());
        engine
    }

    pub fn running(&self) -> bool {
        self.running
    }

    pub fn update(&self) {}

    pub fn render(&mut self) {
        let mut target = self.display.draw();
        target.clear_color(0.5, 0.2, 0.7, 1.0);

        if !self.game_states.is_empty() {
            let mut index = self.game_states.len() - 1;
            for i in (0..self.game_states.len()).rev() {
                if !self.game_states.get(i).unwrap().render_through() {
                    index = i;
                    break;
                }
            }

            for i in index..self.game_states.len() {
                self.game_states.get(i).unwrap().render(&mut target, &self.mesh_factory, &self.program_cache, &0.0);
            }
        }

        assert!(target.finish().is_ok());
    }

    pub fn handle_events(&mut self) {
        if self.game_states.is_empty() || self.input_handler.quit() {
            self.running = false;
        } else {
            self.input_handler.update(self.game_states.last_mut().unwrap().as_mut());
        }
    }

    pub fn push_state<T: GameState + 'static>(&mut self, game_state: T) {
        self.game_states.push(Box::from(game_state));
    }

    pub fn pop_state(&mut self) {
        self.game_states.pop();
    }
}