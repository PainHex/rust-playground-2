use glium_sdl2::SDL2Facade;
use glium::vertex::VertexBuffer;
use glium::index::*;
use engine::math::vec::{Vec2, ToArr};

use super::vertex::Vertex;

pub struct Factory<'a> {
    display: &'a SDL2Facade,
}

impl<'a> Factory<'a> {
    pub fn new(display: &'a SDL2Facade) -> Factory<'a> {
        Factory {
            display: display,
        }
    }

    pub fn rect(&self, centre: &Vec2, size: &Vec2) -> (VertexBuffer<Vertex>, NoIndices) {
        let min = centre - size;
        let max = centre + size;

        let data = &[
            Vertex {
                position: min.to_arr(),
                texcoords: [0.0, 0.0]
            },
            Vertex {
                position: [min.x, max.y],
                texcoords: [0.0, 1.0]
            },
            Vertex {
                position: max.to_arr(),
                texcoords: [1.0, 1.0]
            },
            Vertex {
                position: [max.x, min.y],
                texcoords: [1.0, 0.0]
            },
        ];

        self.build_vbo_from_verts(data)
    }

    pub fn build_vbo_from_verts(&self, verts: &[Vertex]) -> (VertexBuffer<Vertex>, NoIndices) {
        let vertex_buffer = VertexBuffer::new(self.display, verts).unwrap();
        let indices = NoIndices(PrimitiveType::TriangleFan);
        (vertex_buffer, indices)
    }

    pub fn dispose(self) {}
}