use cgmath::*;

pub type Vec2 = Vector2<f32>;
pub type IVec2 = Vector2<i32>;

pub trait ToArr {
    type Output;
    fn to_arr(&self) -> Self::Output;
}

impl ToArr for Vec2 {
    type Output = [f32; 2];
    fn to_arr(&self) -> Self::Output {
        (*self).into()
    }
}

impl ToArr for IVec2 {
    type Output = [i32; 2];
    fn to_arr(&self) -> Self::Output {
        (*self).into()
    }
}