use sdl2::EventPump;
use sdl2::event::Event;
use sdl2::event::Event::*;
use sdl2::keyboard::*;
use sdl2::mouse::Mouse;
use std::collections::{HashSet};

use engine::math::vec::IVec2;
use game::game_states::GameState;

pub struct InputHandler {
    pump: EventPump,
    key_states: HashSet<Scancode>,
    mouse_states: HashSet<Mouse>,
    mouse_position: IVec2,
    pub quit: bool,
}

impl InputHandler {
    pub fn new(pump: EventPump) -> InputHandler {
        InputHandler {
            pump: pump,
            key_states: HashSet::new(),
            mouse_states: HashSet::new(),
            mouse_position: IVec2::new(0, 0),
            quit: false,
        }
    }

    pub fn update(&mut self, game_state: &mut GameState) {
        let mut wrapped_event = self.pump.poll_event();
        while wrapped_event.is_some() {
            let  event = wrapped_event.unwrap();
            match event {

                KeyDown {..} => self.key_states = self.pump.keyboard_state().pressed_scancodes().collect(),
                KeyUp {..} => self.key_states = self.pump.keyboard_state().pressed_scancodes().collect(),

                MouseButtonDown {mouse_btn, ..} => { self.mouse_states.insert(mouse_btn); },
                MouseButtonUp   {mouse_btn, ..} => { self.mouse_states.remove(&mouse_btn); },

                Event::Quit {..} => { self.quit = true },

                MouseMotion {x,y, ..} => {
                    self.mouse_position.x = x;
                    self.mouse_position.y = y;
                },
                _ => {}
            }

            wrapped_event = self.pump.poll_event();
        }
    }

    pub fn quit(&self) -> bool {
        self.quit
    }
}

