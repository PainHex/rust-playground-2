use sdl2;

use engine::engine::Engine as GameEngine;

pub fn run() {
    use glium_sdl2::DisplayBuild;
    let mut sdl_context = sdl2::init().unwrap();

    let video_context = sdl_context.video().unwrap();
    video_context.gl_attr().set_depth_size(24);

    let display = video_context.window("Rust Playground", 800, 600)
        .resizable()
        .build_glium()
        .unwrap();

    let mut timer: sdl2::TimerSubsystem = sdl_context.timer().unwrap();
    let mut engine = GameEngine::new(&mut sdl_context, &display);

    let frame_time: f32 = 1000.0 / 60.0;
    let mut last_tick = timer.ticks();
    let mut tick_delta = 0.0;

    while engine.running() {
        let current_tick = timer.ticks();
        let updates = 0;
        tick_delta += (current_tick - last_tick) as f32;
        last_tick = current_tick;

        engine.handle_events();

        while tick_delta > frame_time && updates < 10 {
            tick_delta -= frame_time;
            engine.update();
        }
        engine.render();
    }
}
