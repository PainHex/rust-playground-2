use glium::{Frame, Surface};
use glium;

use super::GameState;
use engine::program_cache::ProgramCache;
use engine::rendering::factory::Factory;
use engine::math::vec::{Vec2};

pub struct MenuState {
}

impl MenuState {
    pub fn new() -> MenuState {
        MenuState {

        }
    }
}

impl GameState for MenuState {
    fn update(&mut self) {
        //Do nothing for now...
    }

    fn render(&self, surface: &mut Frame, factory: &Factory, program_cache: &ProgramCache, time: &f32) {
        let vbo = factory.rect(&Vec2::new(0.0, 0.0), &Vec2::new(0.6, 0.2));

        surface.draw(&vbo.0, &vbo.1, program_cache.get_program(&"basic".to_string()), &glium::uniforms::EmptyUniforms,
                     &Default::default()).unwrap();
    }

    fn render_through(&self) -> bool {
        false
    }
}