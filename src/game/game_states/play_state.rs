use glium::{Frame};

use super::GameState;
use engine::program_cache::ProgramCache;
use engine::rendering::factory::Factory;

pub struct PlayState {

}

impl GameState for PlayState {
    fn update(&mut self) {
        //Nothing yet
    }

    fn render(&self, surface: &mut Frame, factory: &Factory, program_cache: &ProgramCache, time: &f32) {

    }

    fn render_through(&self) -> bool {
        false
    }
}