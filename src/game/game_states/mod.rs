pub mod menu_state;
pub mod play_state;

use glium::{Frame};

use engine::program_cache::ProgramCache;
use engine::rendering::factory::Factory;

pub trait GameState {
    fn update(&mut self, );

    fn render(&self, surface: &mut Frame, factory: &Factory, program_cache: &ProgramCache, time: &f32);

    fn render_through(&self) -> bool;
}