extern crate framework;

pub use framework::run;

fn main() {
    run();
}
