#[macro_use]
extern crate glium;
extern crate glium_sdl2;
extern crate sdl2;
extern crate cgmath;

#[cfg(test)] extern crate mockers;

pub mod game;
pub mod engine;
use game::game as Game;

pub fn run() {
    Game::run();
}
